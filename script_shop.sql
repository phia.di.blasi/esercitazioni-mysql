CREATE DATABASE shop;

	CREATE TABLE shop.venditore(
		id_venditore int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		nome varchar(16) NOT NULL,
		email varchar(32) NOT NULL,
		telefono varchar(10) NOT NULL,
		indirizzo varchar(127) NOT NULL,
		stato varchar(32) NOT NULL,
		citta varchar(32) NOT NULL,
		cap varchar(5) NOT NULL,
		data_registrazione date NOT NULL
	);

	CREATE TABLE shop.prodotto(
		id_prodotto int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		nome varchar(32) NOT NULL,
		descrizione varchar(127) NOT NULL,
		prezzo decimal(8,2) NOT NULL,
		quantita int NOT NULL,
		disponibile boolean NOT NULL,
		categoria varchar(127) NOT NULL,
		valutazione_media decimal(3,2) NOT NULL
	);

	CREATE TABLE shop.acquirente(
		id_acquirente int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		nome varchar(16) NOT NULL,
		email varchar(32) NOT NULL,
		telefono varchar(10) NOT NULL,
		indirizzo varchar(127) NOT NULL,
		stato varchar(32) NOT NULL,
		citta varchar(32) NOT NULL,
		cap varchar(5) NOT NULL,
		data_registrazione date NOT NULL
	);

	CREATE TABLE shop.ordine(
		id_ordine int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		id_acquirente int NOT NULL,
		totale decimal(8,2) NOT NULL,
		data_ordine date NOT NULL,
		stato varchar(32), 
		CONSTRAINT FK_acquirente FOREIGN KEY (id_acquirente) REFERENCES acquirente(id_acquirente)
	);

	CREATE TABLE shop.dettaglio_ordine(
		id_dettaglio int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		id_ordine int NOT NULL,
		id_prodotto int NOT NULL,
		quantita int NOT NULL,
		prezzo_unitario decimal(8,2) NOT NULL,
		totale_riga decimal(8,2) DEFAULT (quantita*prezzo_unitario),
		CONSTRAINT FK_ordine FOREIGN KEY (id_ordine) REFERENCES ordine(id_ordine),
		CONSTRAINT FK_prodotto FOREIGN KEY (id_prodotto) REFERENCES prodotto(id_prodotto)
	);

	CREATE TABLE shop.recensione_prodotto(
		id_recensione int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		id_prodotto int NOT NULL,
		id_acquirente int NOT NULL,
		valutazione int NOT NULL,
		testo_recensione varchar(255),
		data_recensione date NOT NULL,
		CONSTRAINT prodotto FOREIGN KEY (id_prodotto) REFERENCES prodotto(id_prodotto),
		CONSTRAINT acquirente FOREIGN KEY (id_acquirente) REFERENCES acquirente(id_acquirente)
	);

	CREATE TABLE shop.recensione_venditore(
		id_recensione int AUTO_INCREMENT NOT NULL PRIMARY KEY,
		id_venditore int NOT NULL,
		id_acquirente int NOT NULL,
		valutazione int NOT NULL,
		testo_recensione varchar(255),
		data_recensione date NOT NULL,
		CONSTRAINT ce_venditore FOREIGN KEY (id_venditore) REFERENCES venditore(id_venditore),
		CONSTRAINT ce_acquirente FOREIGN KEY (id_acquirente) REFERENCES acquirente(id_acquirente)
	);
    
    INSERT INTO shop.venditore (nome, email, telefono, indirizzo, stato, citta, cap, data_registrazione)
VALUES ('Necchi', 'necchi@example.com', '1234567890', 'Via Roma 123', 'Italia', 'Milano', '20100', '2024-04-19');
 
 INSERT INTO shop.venditore (nome, email, telefono, indirizzo, stato, citta, cap, data_registrazione)
	VALUES ('Seletti', 'necchi@email.com' , '3334444444' , 'via roma n 111' , 'Italia' , 'Firenze' , '20890' , '2003-06-10' );
    
INSERT INTO shop.venditore (nome, email, telefono, indirizzo, stato, citta, cap, data_registrazione)
	VALUES ('Divani Comodi','comodissimi@email.com','3331234567','via torino n 81','Roma','Milano','22876','2012-02-22');
	
INSERT INTO shop.venditore (nome, email, telefono, indirizzo, stato, citta, cap, data_registrazione)
  VALUES ('Biscottosi','biscottosi@email.com','3456677231','via popoli n 55','Italia','Milano','21001','2020-10-11');
	
INSERT INTO shop.venditore (nome, email, telefono, indirizzo, stato, citta, cap, data_registrazione)
  VALUES  ('Amazon','amazon@email.com','3809988776','via papireto n 32','Italia','Torino','20999','1988-12-02');
       
       
