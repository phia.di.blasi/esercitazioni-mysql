create database job_db;

CREATE TABLE job_db.job(
job_id varchar(20) primary key,
descrizione varchar(100),
min_salary float,
max_salary float
);

CREATE TABLE job_db.reparto(
reparto_id int primary key,
nome varchar (20),
descrizione varchar (100)
);

CREATE TABLE job_db.impiegato(
impiegato_id int auto_increment primary key,
nome varchar (20),
cognome varchar (20),
job_id varchar(20),
foreign key (job_id) references job_db.job (job_id),
reparto_id int,
foreign key (reparto_id) references job_db.reparto (reparto_id)
);

insert into job_db.job values (
	"IT_DEV",
    "sviluppa applicazioni",
    20000,
    28000
);

insert into job_db.job values (
	"IT_CONS",
    "consulenza",
    25000,
    45000
);

insert into job_db.reparto values (
	30,
    "banking",
	"le banche"
);

insert into job_db.impiegato values (
	118,
	"Mario",
	"Rossi",
	"IT_DEV",
	30
);

update job_db.impiegato set job_id = "IT_CONS"
where impiegato_id = 118 and reparto_id = 30 and job_id not like "SH%";

select * from job_db.impiegato;

update job_db.impiegato set job_id = "IT_HWJ"
where impiegato_id = 118 and reparto_id = 30 and job_id not like "SH%";




