CREATE DATABASE supermarket;

CREATE TABLE supermarket.prodotto(
codice_a_barre numeric primary key,
quantita int,
prezzo float
);

CREATE TABLE supermarket.carrello(
id_carrello numeric primary key
);

CREATE TABLE supermarket.carrello_prodotto(
id numeric primary key,
codice_a_barre numeric,
id_carrello numeric,
foreign key (codice_a_barre) references supermarket.prodotto(codice_a_barre),
foreign key (id_carrello) references supermarket.carrello(id_carrello)
);

CREATE TABLE supermarket.reparto(
id_reparto numeric primary key,
numero int,
numero_scaffali int
);

ALTER TABLE supermarket.prodotto ADD COLUMN id_reparto numeric AFTER codice_a_barre;

ALTER TABLE supermarket.prodotto ADD foreign key (id_reparto) references supermarket.reparto(id_reparto);

CREATE TABLE supermarket.impiegato(
id_impiegato numeric primary key,
id_tipo numeric,
foreign key (id_tipo) references supermarket.tipo_impiego(id_tipo),
codice_fiscale varchar (20),
nome varchar (20),
cognome varchar (20)
);

ALTER TABLE supermarket.impiegato ADD COLUMN id_reparto numeric;
ALTER TABLE supermarket.impiegato ADD foreign key (id_reparto) references supermarket.reparto(id_reparto);

CREATE TABLE supermarket.tipo_impiego(
id_tipo numeric primary key,
nome_tipo varchar (30)
);

CREATE TABLE supermarket.magazzino(
id_magazzino numeric primary key,
indirizzo varchar (50)
);

ALTER TABLE supermarket.impiegato ADD COLUMN id_magazzino numeric;
ALTER TABLE supermarket.impiegato ADD foreign key (id_magazzino) references supermarket.magazzino(id_magazzino);

CREATE TABLE supermarket.cliente(
codice_fiscale varchar(20) primary key,
id_carrello numeric,
foreign key (id_carrello) references supermarket.carrello(id_carrello),
nome varchar (20),
cognome varchar (20),
carta_punti varchar (50)
);

ALTER TABLE supermarket.carrello ADD COLUMN codice_fiscale varchar(20);
ALTER TABLE supermarket.carrello ADD foreign key (codice_fiscale) references supermarket.cliente(codice_fiscale);

CREATE TABLE supermarket.prodotto_cliente(
id numeric primary key,
codice_a_barre numeric,
codice_fiscale varchar(20),
foreign key (codice_a_barre) references supermarket.prodotto(codice_a_barre),
foreign key (codice_fiscale) references supermarket.cliente(codice_fiscale)
);

CREATE TABLE supermarket.fornitore(
id_fornitore numeric primary key,
indirizzo varchar (50),
nome varchar (30)
);

CREATE TABLE supermarket.fornitore_magazzino(
id numeric primary key,
id_fornitore numeric,
id_magazzino numeric,
foreign key (id_fornitore) references supermarket.fornitore(id_fornitore),
foreign key (id_magazzino) references supermarket.magazzino(id_magazzino)
);

ALTER TABLE supermarket.reparto ADD COLUMN codice varchar (20);

ALTER TABLE supermarket.fornitore DROP COLUMN indirizzo;











