CREATE DATABASE campionati;

CREATE TABLE campionati.campionato(
id_campionato numeric primary key,
nome varchar (20),
anno varchar (4)
);

CREATE TABLE campionati.arbitro(
codice_fiscale varchar (20) primary key,
nome varchar (20),
cognome varchar (20)
);

CREATE TABLE campionati.luogo(
id_luogo varchar (20) primary key,
nome varchar (20),
citta varchar (20)
);

CREATE TABLE campionati.giocatore(
id_giocatore varchar (20) primary key,
nome varchar (20),
cognome varchar (20),
data_nascita date,
luogo_nascita varchar (20)
);

CREATE TABLE campionati.squadra(
id_squadra varchar (20) primary key,
nome varchar (20),
citta varchar (20)
);

CREATE TABLE campionati.partita(
	id_partita int AUTO_INCREMENT primary key,
    data_partita date,
    id_campionato numeric,
    id_luogo varchar (20),
    id_squadra_1 varchar (20),
    id_squadra_2 varchar (20),
    punti_squadra_1 int,
    punti_squadra_2 int,
    foreign key (id_campionato) references campionato(id_campionato),
    foreign key (id_luogo) references luogo(id_luogo),
    foreign key (id_squadra_1) references squadra(id_squadra),
    foreign key (id_squadra_2) references squadra(id_squadra) 
);

CREATE TABLE campionati.direzione(
	codice_fiscale varchar (20),
    id_partita int,
    foreign key (codice_fiscale) references arbitro(codice_fiscale), 
    foreign key (id_partita) references partita(id_partita)
);

CREATE TABLE campionati.formazione(
	anno varchar (4) primary key,
	id_giocatore varchar (20),
    id_squadra varchar (20),
    numero int,
    foreign key (id_giocatore) references giocatore(id_giocatore), 
    foreign key (id_squadra) references squadra(id_squadra)
);

CREATE TABLE campionati.segna(
	minuto varchar(5) primary key,
	id_giocatore varchar (20),
    id_partita int,
    foreign key (id_giocatore) references giocatore(id_giocatore), 
    foreign key (id_partita) references partita(id_partita)
);

ALTER TABLE campionati.luogo ADD COLUMN regione varchar (20);
ALTER TABLE campionati.luogo ADD COLUMN provincia varchar (20);

ALTER TABLE campionati.squadra ADD COLUMN colore_squadra varchar (20);

ALTER TABLE campionati.luogo MODIFY COLUMN nome varchar(255);


